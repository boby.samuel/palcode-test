import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TimetrackingModule } from './timetracking/timetracking.module';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { ResponseInterceptor } from "./interceptor/response.interceptor"

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'admin',
      password: 'admin',
      database: 'timetracking',
      autoLoadEntities: true,
      synchronize: true,
    }),
    TimetrackingModule,
  ],
  controllers: [AppController],
  providers: [
    AppService, 
    {
      provide: APP_INTERCEPTOR,
      useClass: ResponseInterceptor,
    }
  ],
})
export class AppModule {}
