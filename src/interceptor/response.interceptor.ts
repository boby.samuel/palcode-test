import {
  CallHandler,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
  NestInterceptor,
} from "@nestjs/common";
import { Observable, catchError, map } from "rxjs";

export const StatusMessage = {
  200: "Sukses",
  201: "Sukses",
  400: "Bad Request",
  404: "Tidak Ditemukan",
  422: "Validation Error",
  500: "Internal Server Error",
};


export interface IOutputResponse {
  code: number;
  status: boolean;
  data?: Record<string, any> | Array<Record<string, any>> | null;
  error?: Record<string, any> | string;
}



@Injectable()
export class ResponseInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      map(async (data) => {
        const res = context.switchToHttp().getResponse();

        const formatRes: IOutputResponse = {
          code: HttpStatus.OK,
          status: true,
          data,
        };

        return formatRes;
      }),
      catchError((err) => {
        const res: IOutputResponse = {
          code: HttpStatus.INTERNAL_SERVER_ERROR,
          status: false,
          error: err.message,
        };

        throw new HttpException(res, res.code);
      }),
    );
  }
}
