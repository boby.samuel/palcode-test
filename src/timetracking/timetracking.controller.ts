import { Body, Controller, Get, Post } from '@nestjs/common';
import { CreateTimeLogDto } from './dto';
import { TimetrackingService } from './timetracking.service';

@Controller('timetracking')
export class TimetrackingController {
    constructor(
        private timeTrackingService: TimetrackingService
    ){}

    @Post()
    logDuration(@Body() payload: CreateTimeLogDto) {
        try {
            return this.timeTrackingService.create(payload)
        }
        catch(err) {
            throw err
        }
    }

    @Get()
    getAll() {
        try {
            return this.timeTrackingService.findAll()
        }
        catch(err) {
            throw err
        }
    }
}
