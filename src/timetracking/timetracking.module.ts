import { Module } from '@nestjs/common';
import { TimetrackingController } from './timetracking.controller';
import { TimetrackingService } from './timetracking.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TimeTracking } from './entities';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            TimeTracking
        ])
    ],
    controllers: [TimetrackingController],
    providers: [TimetrackingService]
})
export class TimetrackingModule {
}
